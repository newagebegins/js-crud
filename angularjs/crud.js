angular.module('app', ['ngRoute'])

.config(function($routeProvider) {
  $routeProvider.
    when('/:entityType', {
      templateUrl: 'entity-list.html',
      controller: 'EntityController'
    }).
    otherwise({
      redirectTo: '/people'
    });
})

.constant('entityTypes', {
  people: {
    idPropertyName: 'person_id',
    pageTitle: 'People',
    addEntityString: 'Add person',
    editEntityString: 'Edit person #%ID%',
    fields: {
      id: {
        label: 'ID',
        type: 'hidden'
      },
      firstName: {
        label: 'First name',
        type: 'text',
        required: true
      },
      lastName: {
        label: 'Last name',
        type: 'text',
        required: true
      },
      middleName: {
        label: 'Middle name',
        type: 'text'
      },
      email: {
        label: 'Email',
        type: 'email'
      },
      phone: {
        label: 'Phone number',
        type: 'text'
      }
    }
  },

  companies: {
    idPropertyName: 'company_id',
    pageTitle: 'Companies',
    addEntityString: 'Add company',
    editEntityString: 'Edit company #%ID%',
    fields: {
      id: {
        label: 'ID',
        type: 'hidden'
      },
      name: {
        label: 'Name',
        type: 'text',
        required: true
      },
      description: {
        label: 'Description',
        type: 'textarea'
      },
      logo: {
        label: 'Logo URL',
        type: 'text',
        role: 'image'
      }
    }
  },

  departments: {
    idPropertyName: 'department_id',
    pageTitle: 'Departments',
    addEntityString: 'Add department',
    editEntityString: 'Edit department #%ID%',
    fields: {
      id: {
        label: 'ID',
        type: 'hidden'
      },
      name: {
        label: 'Name',
        type: 'text',
        required: true
      },
      company: {
        label: 'Company',
        type: 'select',
        selectOptions: 'company.getField("id") as company.label() for company in companies'
      }
    }
  },

  positions: {
    idPropertyName: 'position_id',
    pageTitle: 'Positions',
    addEntityString: 'Add position',
    editEntityString: 'Edit position #%ID%',
    fields: {
      id: {
        label: 'ID',
        type: 'hidden'
      },
      name: {
        label: 'Name',
        type: 'text',
        required: true
      },
      salary: {
        label: 'Salary',
        type: 'number'
      }
    }
  },

  users: {
    idPropertyName: 'user_id',
    pageTitle: 'Users',
    addEntityString: 'Add user',
    editEntityString: 'Edit user #%ID%',
    fields: {
      id: {
        label: 'ID',
        type: 'hidden'
      },
      nickname: {
        label: 'Nickname',
        type: 'text',
        required: true
      },
      department: {
        label: 'Department',
        type: 'select',
        selectOptions: 'department.getField("id") as department.label() for department in departments'
      },
      person: {
        label: 'Person',
        type: 'select',
        selectOptions: 'person.getField("id") as person.label() for person in people'
      },
      position: {
        label: 'Position',
        type: 'select',
        selectOptions: 'position.getField("id") as position.label() for position in positions'
      },
      superUser: {
        label: 'Super user',
        type: 'checkbox'
      }
    }
  }
})

.factory('typeMap', function(Person, User, Company, Department, Position) {
  return {
    people: Person,
    users: User,
    companies: Company,
    departments: Department,
    positions: Position
  };
})

.factory('Entity', function() {
  function Entity(data) {
    this.data = data || {};
    this.beingDeleted = false;
    this.formData = {};
  }

  Entity.prototype.label = function() {};
  Entity.prototype.getField = function(fieldName, returnRawValue) {};
  Entity.prototype.setField = function(fieldName, val) {};

  return Entity;
})

.factory('Person', function(Entity) {
  function Person(data) {
    Entity.call(this, data);
    this.type = 'people';
  }

  Person.prototype = Object.create(Entity.prototype);
  Person.prototype.constructor = Person;

  Person.prototype.label = function() {
    return this.data.first_name + ' ' + this.data.last_name;
  };

  Person.prototype.getField = function(fieldName, returnRawValue) {
    var field = '';

    switch (fieldName) {
      case 'id':
        field = this.data.person_id;
        break;

      case 'firstName':
        field = this.data.first_name;
        break;

      case 'lastName':
        field = this.data.last_name;
        break;

      case 'middleName':
        field = this.data.middle_name;
        break;

      case 'email':
        field = this.data.email;
        break;

      case 'phone':
        field = this.data.phone_number;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }

    return field;
  };

  Person.prototype.setField = function(fieldName, val) {
    switch (fieldName) {
      case 'id':
        this.data.person_id = val;
        break;

      case 'firstName':
        this.data.first_name = val;
        break;

      case 'lastName':
        this.data.last_name = val;
        break;

      case 'middleName':
        this.data.middle_name = val;
        break;

      case 'email':
        this.data.email = val;
        break;

      case 'phone':
        this.data.phone_number = val;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }
  };

  return Person;
})

.factory('User', function(Entity) {
  function User(data) {
    Entity.call(this, data);
    this.type = 'users';
  }

  User.prototype = Object.create(Entity.prototype);
  User.prototype.constructor = User;

  User.prototype.label = function() {
    return this.data.nickname;
  };

  User.prototype.getField = function(fieldName, returnRawValue) {
    var field = '';

    switch (fieldName) {
      case 'id':
        field = this.data.user_id;
        break;

      case 'nickname':
        field = this.data.nickname;
        break;

      case 'department':
        if (returnRawValue) {
          field = this.department.getField('id');
        } else {
          field = this.department.label();
        }
        break;

      case 'person':
        if (returnRawValue) {
          field = this.person.getField('id');
        } else {
          field = this.person.label();
        }
        break;

      case 'position':
        if (returnRawValue) {
          field = this.position.getField('id');
        } else {
          field = this.position.label();
        }
        break;

      case 'superUser':
        if (returnRawValue) {
          field = this.data.super_user;
        } else {
          field = this.data.super_user ? 'Yes' : 'No';
        }
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }

    return field;
  };

  User.prototype.setField = function(fieldName, val) {
    switch (fieldName) {
      case 'id':
        this.data.user_id = val;
        break;

      case 'nickname':
        this.data.nickname = val;
        break;

      case 'department':
        this.data.department_id = val;
        break;

      case 'person':
        this.data.person_id = val;
        break;

      case 'position':
        this.data.position_id = val;
        break;

      case 'superUser':
        this.data.super_user = val;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }
  };

  return User;
})

.factory('Company', function(Entity) {
  function Company(data) {
    Entity.call(this, data);
    this.type = 'companies';
  }

  Company.prototype = Object.create(Entity.prototype);
  Company.prototype.constructor = Company;

  Company.prototype.label = function() {
    return this.data.company_name;
  };

  Company.prototype.getField = function(fieldName, returnRawValue) {
    var field = '';

    switch (fieldName) {
      case 'id':
        field = this.data.company_id;
        break;

      case 'name':
        field = this.data.company_name;
        break;

      case 'description':
        field = this.data.description;
        break;

      case 'logo':
        field = this.data.logo;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }

    return field;
  };

  Company.prototype.setField = function(fieldName, val) {
    switch (fieldName) {
      case 'id':
        this.data.company_id = val;
        break;

      case 'name':
        this.data.company_name = val;
        break;

      case 'description':
        this.data.description = val;
        break;

      case 'logo':
        this.data.logo = val;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }    
  };

  return Company;
})

.factory('Department', function(Entity) {
  function Department(data) {
    Entity.call(this, data);
    this.type = 'departments';
  }

  Department.prototype = Object.create(Entity.prototype);
  Department.prototype.constructor = Department;

  Department.prototype.label = function() {
    return this.data.department_name + ' (' + this.company.label() + ')';
  };

  Department.prototype.getField = function(fieldName, returnRawValue) {
    var field = '';

    switch (fieldName) {
      case 'id':
        field = this.data.department_id;
        break;

      case 'name':
        field = this.data.department_name;
        break;

      case 'company':
        if (returnRawValue) {
          field = this.company.getField('id');
        } else {
          field = this.company.label();
        }
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }

    return field;
  };

  Department.prototype.setField = function(fieldName, val) {
    switch (fieldName) {
      case 'id':
        this.data.department_id = val;
        break;

      case 'name':
        this.data.department_name = val;
        break;

      case 'company':
        this.data.company_id = val;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }
  };

  return Department;
})


.factory('Position', function(Entity) {
  function Position(data) {
    Entity.call(this, data);
    this.type = 'positions';
  }

  Position.prototype = Object.create(Entity.prototype);
  Position.prototype.constructor = Position;

  Position.prototype.label = function() {
    return this.data.position_name;
  };

  Position.prototype.getField = function(fieldName, returnRawValue) {
    var field = '';

    switch (fieldName) {
      case 'id':
        field = this.data.position_id;
        break;

      case 'name':
        field = this.data.position_name;
        break;

      case 'salary':
        field = this.data.salary;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }

    return field;
  };

  Position.prototype.setField = function(fieldName, val) {
    switch (fieldName) {
      case 'id':
        this.data.position_id = val;
        break;

      case 'name':
        this.data.position_name = val;
        break;

      case 'salary':
        this.data.salary = val;
        break;

      default:
        throw 'Unrecognized field name: ' + fieldName;
    }    
  };

  return Position;
})

.factory('appStorage', function($q, $http, $timeout, entityTypes,
                                Person, User, Company, Department, Position) {
  var cachedAppData;

  return {
    getEntities: getEntities,
    saveEntity: saveEntity,
    deleteEntity: deleteEntity
  };

  function getPersonById(appData, id) {
    var person;
    for (var idx = 0; idx < appData.people.length; ++idx) {
      if (appData.people[idx].person_id == id) {
        person = new Person(appData.people[idx]);
        break;
      }
    }
    return person;
  }

  function getCompanyById(appData, id) {
    var company;
    for (var idx = 0; idx < appData.companies.length; ++idx) {
      if (appData.companies[idx].company_id == id) {
        company = new Company(appData.companies[idx]);
        break;
      }
    }
    return company;
  }

  function getDepartmentById(appData, id) {
    var company;
    for (var idx = 0; idx < appData.departments.length; ++idx) {
      if (appData.departments[idx].department_id == id) {
        department = new Department(appData.departments[idx]);
        break;
      }
    }
    return department;
  }

  function getPositionById(appData, id) {
    var company;
    for (var idx = 0; idx < appData.positions.length; ++idx) {
      if (appData.positions[idx].position_id == id) {
        position = new Position(appData.positions[idx]);
        break;
      }
    }
    return position;
  }
  
  function getEntities(entityType) {
    var deferred = $q.defer();

    getAppData().then(function(appData) {
      var entities = [];
      angular.forEach(appData[entityType], function(data, key) {
        var entity;
        switch (entityType) {
          case 'people': {
            entity = new Person(data);
            break;
          }

          case 'users': {
            entity = new User(data);
            entity.person = getPersonById(appData, data.person_id);
            entity.department = getDepartmentById(appData, data.department_id);
            entity.department.company = getCompanyById(appData, entity.department.data.company_id);
            entity.position = getPositionById(appData, data.position_id);
            break;
          }

          case 'companies': {
            entity = new Company(data);
            break;
          }

          case 'departments': {
            entity = new Department(data);
            entity.company = getCompanyById(appData, data.company_id);
            break;
          }

          case 'positions': {
            entity = new Position(data);
            break;
          }
        }
        entities.push(entity);
      });
      deferred.resolve(entities);
    });

    return deferred.promise;
  }

  function saveEntity(entity) {
    var deferred = $q.defer();

    $timeout(function() {
      getAppData().then(function(appData) {
        angular.forEach(entity.formData, function(value, key) {
          entity.setField(key, value);
        });
        var idPropertyName = entityTypes[entity.type].idPropertyName;
        if (!entity.getField('id')) {
          entity.data[idPropertyName] = getNextEntityID(appData[entity.type], idPropertyName);
          appData[entity.type].push(entity.data);
        } else {
          for (var index = 0; index < appData[entity.type].length; ++index) {
            if (appData[entity.type][index][idPropertyName] == entity.data[idPropertyName]) {
              appData[entity.type][index] = entity.data;
              break;
            }
          }
        }
        saveAppData(appData);
        deferred.resolve(entity);
      });
    }, 1500);

    return deferred.promise;
  }

  function deleteEntity(entity) {
    var deferred = $q.defer();

    $timeout(function() {
      getAppData().then(function(appData) {
        _deleteEntity(appData, entity.data, entity.type);
        saveAppData(appData);
        deferred.resolve(entity);
      });
    }, 1500);

    return deferred.promise;
  }

  function _deleteEntity(appData, entity, entityType) {

    // Delete related entities

    switch (entityType) {
      case 'people': {
        _deleteRelatedEntities(appData, entity, 'person_id', 'users');
        break;
      }

      case 'users': {
        break;
      }

      case 'departments': {
        _deleteRelatedEntities(appData, entity, 'department_id', 'users');
        break;
      }

      case 'positions': {
        _deleteRelatedEntities(appData, entity, 'position_id', 'users');
        break;
      }

      case 'companies': {
        _deleteRelatedEntities(appData, entity, 'company_id', 'departments');
        break;
      }
    }

    // Delete the actual entity

    var idPropertyName = entityTypes[entityType].idPropertyName;
    for (var index = 0; index < appData[entityType].length; ++index) {
      if (appData[entityType][index][idPropertyName] == entity[idPropertyName]) {
        appData[entityType].splice(index, 1);        
        break;
      }
    }
  }

  function _deleteRelatedEntities(appData, parent, parentIdProperty, childType) {
    var toDelete = [];
    angular.forEach(appData[childType], function(child, key) {
      if (child[parentIdProperty] == parent[parentIdProperty]) {
        toDelete.push(child);
      }
    });
    angular.forEach(toDelete, function(child, key) {
      _deleteEntity(appData, child, childType);
    });
  }

  function getAppData() {
    var gettingData = $q.defer();

    if (cachedAppData) {
        gettingData.resolve(cachedAppData);
    } else {
      var localAppDataString = localStorage.getItem('appData');

      if (localAppDataString) {
        var appData = JSON.parse(localAppDataString);
        cachedAppData = appData;
        gettingData.resolve(appData);
      } else {
        $http.get('seed.json').then(function(response) {
          var appData = response.data;
          cachedAppData = appData;
          saveAppData(appData);
          gettingData.resolve(appData);
        }, function(response) {
          alert("Failed to fetch seed.json!");      
          gettingData.reject();
        });
      }
    }

    return gettingData.promise;
  }

  function saveAppData(appData) {
    localStorage.setItem("appData", JSON.stringify(appData));
  }

  function getNextEntityID(entityArray, idPropertyName) {
    var maxId = 0;
    for (var index = 0; index < entityArray.length; ++index) {
      var entity = entityArray[index];
      if (entity[idPropertyName] > maxId) {
        maxId = entity[idPropertyName];
      }
    }
    var nextId = maxId + 1;
    return nextId;
  }
})
.controller('NavbarController', function($scope, $location) { 
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
})

.directive('myModal', function() {
  return {
    restrict: 'A',
    scope: {
      modalState: '=myModal'
    },
    link: function(scope, element, attrs) {
      var modalState = scope.modalState;

      scope.$watch('modalState.isVisible', function(newValue, oldValue) {
        if (newValue != oldValue) {
          element.modal(newValue ? 'show' : 'hide');
        }
      });

      element.on('hide.bs.modal', function() {
        if (modalState.isVisible) {
          scope.$apply('modalState.isVisible = false');
        }
      });

      scope.$watch('modalState.inProgress', function(newValue, oldValue) {
        if (newValue != oldValue) {
          if (newValue) {
            // Forbid closing the modal
            element.on('hide.bs.modal.prevent', function(event) {
              event.preventDefault();
            });
          } else {
            element.off('hide.bs.modal.prevent').modal('hide');
          }
        }
      });
    }
  };
})

.directive('myClickConfirm', function($window) {
  return {
    restrict: 'A',
    scope: {
      action: '&myClickConfirm',
      message: '@myConfirmMessage'
    },
    link: function(scope, element, attrs) {
      element.on('click', function() {
        if ($window.confirm(scope.message)) {
          scope.$apply('action()');
        }
      });
    }
  };
})

.controller('EntityController', function($scope, $routeParams, appStorage, entityTypes, typeMap) {
  var entityType = $routeParams.entityType;

  $scope.entityType = entityTypes[entityType];
  $scope.formEntity = createFormEntity();

  $scope.modalState = {
    isVisible: false,
    inProgress: false
  };

  $scope.addEntity = function() {
    $scope.modalTitle = 'Add ' + $scope.entityType.addEntityString;
    $scope.modalState.isVisible = true;
  };

  $scope.editEntity = function(entity) {
    $scope.modalTitle = $scope.entityType.editEntityString.replace('%ID%', entity.getField('id'));
    $scope.formEntity = angular.copy(entity);
    angular.forEach($scope.entityType.fields, function(field, fieldId) {
      $scope.formEntity.formData[fieldId] = $scope.formEntity.getField(fieldId, true);
    });
    $scope.modalState.isVisible = true;
  };

  $scope.saveEntity = function(formEntity) {
    $scope.modalState.inProgress = true;
    appStorage.saveEntity(formEntity).then(function(entity) {
      $.jGrowl(entity.label() + " has been saved");
      $scope.modalState.inProgress = false;
      $scope.modalState.isVisible = false;
      $scope.formEntity = createFormEntity();
      fetchEntities();
    });
  };

  $scope.deleteEntity = function(entity) {
    entity.beingDeleted = true;
    appStorage.deleteEntity(entity).then(function(entity) {
      $.jGrowl(entity.label() + " has been deleted");
      fetchEntities();
    });
  };

  fetchEntities();

  function fetchEntities() {
    appStorage.getEntities(entityType).then(function(entities) {
      $scope.entities = entities;
    });
    appStorage.getEntities('companies').then(function(companies) {
      $scope.companies = companies;
    });
    appStorage.getEntities('departments').then(function(departments) {
      $scope.departments = departments;
    });
    appStorage.getEntities('people').then(function(people) {
      $scope.people = people;
    });
    appStorage.getEntities('positions').then(function(positions) {
      $scope.positions = positions;
    });
  }

  function createFormEntity() {
    var entity = new typeMap[entityType]();
    return entity;
  }
})

;
