(function () {

var DUMMY_SERVER_DELAY = 1500;

var EntityType = {
  PERSON: "person",
  USER: "user",
  POSITION: "position",
  DEPARTMENT: "department",
  COMPANY: "company",
};

function capitalizeFirstLetter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function saveAppData(appData) {
  localStorage.setItem("appData", JSON.stringify(appData));
}

function getAppData() {
  var gettingData = $.Deferred();
  var appData = localStorage.getItem("appData");
  if (appData) {
    gettingData.resolve(JSON.parse(appData));
  } else {
    localStorage.clear();
    $.getJSON("seed.json")
      .done(function (data, textStatus, jqXHR) {
        saveAppData(data);
        gettingData.resolve(data);
      })
      .fail(function (data, textStatus, jqXHR) {
        alert("Failed to fetch seed.json!");
      });
  }
  return gettingData.promise();
}

function getNextID(dataTable) {
  var nextId = 0;
  for (var id in dataTable) {
    if (dataTable.hasOwnProperty(id)) {
      id = parseInt(id);
      if (id > nextId) {
        nextId = id;
      }
    }
  }
  nextId++;
  return nextId;
}

function clearForm($form) {
  $form.find('input:text, input:password, input[type=email], input[type=number], input:file, select, textarea').val('');
  $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

function saveEntity(appData, entity) {
  var savingEntity = $.Deferred();
  setTimeout(function () {
    if (!entity.id) {
      entity.id = getNextID(appData[entity.type]);
    }
    appData[entity.type][entity.id] = entity;
    saveAppData(appData);
    savingEntity.resolve(entity);
  }, DUMMY_SERVER_DELAY);
  return savingEntity.promise();
}

// Also deletes all associated entities
function _deleteEntity(appData, entity) {
  switch (entity.type) {
    case EntityType.PERSON: {
      $.each(appData[EntityType.USER], function (index, user) {
        if (user.person_id == entity.id) {
          _deleteEntity(appData, user);
        }
      });
      break;
    }

    case EntityType.USER: {
      break;
    }

    case EntityType.POSITION: {
      $.each(appData[EntityType.USER], function (index, user) {
        if (user.position_id == entity.id) {
          _deleteEntity(appData, user);
        }
      });
      break;
    }

    case EntityType.DEPARTMENT: {
      $.each(appData[EntityType.USER], function (index, user) {
        if (user.department_id == entity.id) {
          _deleteEntity(appData, user);
        }
      });
      break;
    }

    case EntityType.COMPANY: {
      $.each(appData[EntityType.DEPARTMENT], function (index, department) {
        if (department.company_id == entity.id) {
          _deleteEntity(appData, department);
        }
      });
      break;
    }

    default: {
      throw new Error("Unexpected entity type: " + entityType);
    }
  }

  delete appData[entity.type][entity.id];
}

function deleteEntity(appData, entity) {
  var deletingEntity = $.Deferred();
  setTimeout(function () {
    _deleteEntity(appData, entity);
    saveAppData(appData);
    deletingEntity.resolve(entity);
  }, DUMMY_SERVER_DELAY);
  return deletingEntity.promise();
}

function getEntityRowHtml(appData, entity) {
  var html = "";
  html += "<tr data-entity-id='" + entity.id + "'>";

  switch (entity.type) {
    case EntityType.PERSON: {
      html += "<td>" + entity.id + "</td>";
      html += "<td>" + entity.first_name + "</td>";
      html += "<td>" + entity.last_name + "</td>";
      html += "<td>" + entity.middle_name + "</td>";
      html += "<td>" + entity.email + "</td>";
      html += "<td>" + entity.phone_number + "</td>";
      break;
    }

    case EntityType.USER: {
      var department = appData.department[entity.department_id];
      var company = appData.company[department.company_id];
      var person = appData.person[entity.person_id];
      var position = appData.position[entity.position_id];
      html += "<td>" + entity.id + "</td>";
      html += "<td>" + entity.nickname + "</td>";
      html += "<td>" + company.company_name + "</td>";
      html += "<td>" + department.department_name + "</td>";
      html += "<td>" + person.first_name + " " + person.last_name + "</td>";
      html += "<td>" + position.position_name + "</td>";
      html += "<td>" + (entity.super_user ? "Yes" : "No") + "</td>";
      break;
    }

    case EntityType.POSITION: {
      html += "<td>" + entity.id + "</td>";
      html += "<td>" + entity.position_name + "</td>";
      html += "<td>" + entity.salary + "</td>";
      break;
    }

    case EntityType.DEPARTMENT: {
      var company = appData.company[entity.company_id];
      html += "<td>" + entity.id + "</td>";
      html += "<td>" + entity.department_name + "</td>";
      html += "<td>" + company.company_name + "</td>";
      break;
    }

    case EntityType.COMPANY: {
      html += "<td>" + entity.id + "</td>";
      html += "<td>" + entity.company_name + "</td>";
      html += "<td>" + entity.description + "</td>";
      html += "<td>" + (entity.logo ? "<img src='" + entity.logo + "' alt='' width='100' />" : "") + "</td>";
      break;
    }

    default: {
      throw new Error("Unexpected entity type: " + entityType);
    }
  }
  
  html += '<td><button class="btn btn-default" data-toggle="modal" data-target="#' + entity.type + '-modal" data-entity-id="' + entity.id + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button> <button class="btn btn-danger" data-delete><span class="spinner glyphicon glyphicon-refresh" aria-hidden="true"></span> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</button></td>';
  html += "</tr>";
  return html;
}

function getEntityListEl(entityType) {
  return $("#" + entityType + "-list");
}

function appendEntityRow(appData, entity) {
  getEntityListEl(entity.type).find("tbody").append(getEntityRowHtml(appData, entity));
}

function getEntityRow(entity) {
  return getEntityListEl(entity.type).find("tr[data-entity-id=" + entity.id + "]");
}

function replaceEntityRow(appData, entity) {
  getEntityRow(entity).replaceWith(getEntityRowHtml(appData, entity));
}

function removeEntityRow(entity) {
  getEntityRow(entity).remove();
}

function getEntityFormData($form, entityType) {
  var entity;

  switch (entityType) {
    case EntityType.PERSON: {
      entity = {
        "type": EntityType.PERSON,
        "first_name": $("#person-form-first-name").val(),
        "last_name": $("#person-form-last-name").val(),
        "middle_name": $("#person-form-middle-name").val(),
        "email": $("#person-form-email").val(),
        "phone_number": $("#person-form-phone").val(),
      };
      break;
    }

    case EntityType.USER: {
      entity = {
        "type": EntityType.USER,
        "nickname": $("#user-form-nickname").val(),
        "department_id": $("#user-form-department-id").val(),
        "person_id": $("#user-form-person-id").val(),
        "position_id": $("#user-form-position-id").val(),
        "super_user": $("#user-form-super-user").prop("checked"),
      };
      break;
    }

    case EntityType.POSITION: {
      entity = {
        "type": EntityType.POSITION,
        "position_name": $("#position-form-name").val(),
        "salary": $("#position-form-salary").val(),
      };
      break;
    }

    case EntityType.DEPARTMENT: {
      entity = {
        "type": EntityType.DEPARTMENT,
        "department_name": $("#department-form-name").val(),
        "company_id": $("#department-form-company-id").val(),
      };
      break;
    }

    case EntityType.COMPANY: {
      entity = {
        "type": EntityType.COMPANY,
        "company_name": $("#company-form-name").val(),
        "description": $("#company-form-description").val(),
        "logo": $("#company-form-logo").val(),
      };
      break;
    }

    default: {
      throw new Error("Unexpected entity type: " + entityType);
    }
  }

  entity.id = $form.data("entityId");

  return entity;
}

function fillEntityFormData(entity) {
  switch (entity.type) {
    case EntityType.PERSON: {
      $("#person-form-first-name").val(entity.first_name);
      $("#person-form-last-name").val(entity.last_name);
      $("#person-form-middle-name").val(entity.middle_name);
      $("#person-form-email").val(entity.email);
      $("#person-form-phone").val(entity.phone_number);
      break;
    }

    case EntityType.USER: {
      $("#user-form-nickname").val(entity.nickname);
      $("#user-form-department-id").val(entity.department_id);
      $("#user-form-person-id").val(entity.person_id);
      $("#user-form-position-id").val(entity.position_id);
      $("#user-form-super-user").prop("checked", entity.super_user);
      break;
    }

    case EntityType.POSITION: {
      $("#position-form-name").val(entity.position_name);
      $("#position-form-salary").val(entity.salary);
      break;
    }

    case EntityType.DEPARTMENT: {
      $("#department-form-name").val(entity.department_name);
      $("#department-form-company-id").val(entity.company_id);
      break;
    }

    case EntityType.COMPANY: {
      $("#company-form-name").val(entity.company_name);
      $("#company-form-description").val(entity.description);
      $("#company-form-logo").val(entity.logo);
      break;
    }

    default: {
      throw new Error("Unexpected entity type: " + entityType);
    }
  }

  return entity;
}

function entityToString(entity) {
  var result = "";

  switch (entity.type) {
    case EntityType.PERSON: {
      result = entity.first_name + " " + entity.last_name;
      break;
    }

    case EntityType.USER: {
      result = entity.nickname;
      break;
    }

    case EntityType.POSITION: {
      result = entity.position_name;
      break;
    }

    case EntityType.DEPARTMENT: {
      result = entity.department_name;
      break;
    }

    case EntityType.COMPANY: {
      result = entity.company_name;
      break;
    }

    default: {
      throw new Error("Unexpected entity type: " + entityType);
    }
  }

  return result;
}

getAppData().done(function (appData) {
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (event) {
    var entityType = $(event.target).attr("href").match(/#(\w+)-list/)[1];
    getEntityListEl(entityType).find("tbody").empty();
    $.each(appData[entityType], function (index, entity) {
      appendEntityRow(appData, entity);
    });      
  })

  $("div[data-entity-modal] form").submit(function () {
    var $form = $(this);
    var $modal = $form.closest("div[data-entity-modal]");
    var entityType = $modal.data("entityModal");

    var $submit = $form.find("button");
    $submit.prop("disabled", true);

    // Disallow closing the modal.
    $modal.on('hide.bs.modal.prevent', function (event) {
      event.preventDefault()
    });

    var formData = getEntityFormData($form, entityType);

    saveEntity(appData, formData).done(function (entity) {
      if ($form.data("entityId")) {
        replaceEntityRow(appData, entity);
        $.jGrowl(capitalizeFirstLetter(entity.type) + " #" + entity.id + " has been updated");
      } else {
        appendEntityRow(appData, entity);
        $.jGrowl(capitalizeFirstLetter(entity.type) + " " + entityToString(entity) + " has been created");
      }
      $submit.prop("disabled", false);
      $modal.off('hide.bs.modal.prevent').modal('hide');
    });

    return false;
  });

  $("div[data-entity-modal]").on("show.bs.modal", function (event) {
    var $trigger = $(event.relatedTarget);
    var $modal = $(this);
    var entityType = $modal.data("entityModal");
    var entityId = $trigger.data("entityId");
    var $form = $modal.find("form");
    clearForm($form);

    if (entityId) {
      $modal.find(".modal-title").text("Edit " + entityType + " #" + entityId);
      $form.data("entityId", entityId);
      var entity = appData[entityType][entityId];
      fillEntityFormData(entity);
    } else {
      $modal.find(".modal-title").text("Add " + entityType);
      $form.removeData("entityId");
    }

    var $companySelect = $form.find("select[data-company-select]");
    if ($companySelect.length > 0) {
      $companySelect.empty();
      $.each(appData[EntityType.COMPANY], function (index, company) {
        $companySelect.append("<option value='" + company.id + "'>" + entityToString(company) + "</option>");
      });
    }

    var $departmentSelect = $form.find("select[data-department-select]");
    if ($departmentSelect.length > 0) {
      $companySelect.on("change", function () {
        $departmentSelect.empty();
        $.each(appData[EntityType.DEPARTMENT], function (index, department) {
          if (department.company_id == $companySelect.val()) {
            $departmentSelect.append("<option value='" + department.id + "'>" + entityToString(department) + "</option>");
          }
        });
      });
      $companySelect.trigger("change");
    }

    var $personSelect = $form.find("select[data-person-select]");
    if ($personSelect.length > 0) {
      $personSelect.empty();
      $.each(appData[EntityType.PERSON], function (index, person) {
        $personSelect.append("<option value='" + person.id + "'>" + entityToString(person) + "</option>");
      });
    }

    var $positionSelect = $form.find("select[data-position-select]");
    if ($positionSelect.length > 0) {
      $positionSelect.empty();
      $.each(appData[EntityType.POSITION], function (index, position) {
        $positionSelect.append("<option value='" + position.id + "'>" + entityToString(position) + "</option>");
      });
    }
  });

  $("body").on("click", "button[data-delete]", function () {
    var $button = $(this);
    var $tr = $button.closest("tr");
    var $tabpanel = $button.closest("[role='tabpanel']");
    var entityType = $tabpanel.data("entityType");
    var entity = appData[entityType][$tr.data("entityId")];
    if (confirm("Are you sure you want to delete " + entityType + " " + entityToString(entity) + "?"))
    {
      $tr.find("button").prop("disabled", true);
      deleteEntity(appData, entity).done(function (entity) {
        removeEntityRow(entity);
        $.jGrowl(entityType + " " + entityToString(entity) + " has been deleted");
      });
    }
  });

  //
  // Tab linking
  //

  var pageUrl = document.location.toString();
  if (pageUrl.match('#')) {
    $('.navbar-nav a[href=#'+pageUrl.split('#')[1]+']').tab('show') ;
  } else {
    $('.navbar-nav a[href=#person-list]').tab('show') ;  
  }

  $('.navbar-nav a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
  })
});

})();
